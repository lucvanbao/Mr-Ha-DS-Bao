{
 "cells": [
  {
   "cell_type": "code",
   "execution_count": 1,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "20\n",
      "12\n",
      "22\n"
     ]
    }
   ],
   "source": [
    "def sum(x, y):\n",
    "    sum = x + y\n",
    "    if sum in range(15, 20):\n",
    "        return 20\n",
    "    else:\n",
    "        return sum\n",
    "\n",
    "print(sum(10, 6))\n",
    "print(sum(10, 2))\n",
    "print(sum(10, 12))\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "Current date and time: \n",
      "2018-08-17 17:47:01\n"
     ]
    }
   ],
   "source": [
    "import datetime\n",
    "now=datetime.datetime.now()\n",
    "print(\"Current date and time: \")\n",
    "print(now.strftime('%Y-%m-%d %H:%M:%S'))\n",
    "#chọn trường thời gian, gán biến now định dang ngày tháng năm, in biến now"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.4"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
